/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.festaRave.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Festa {
    
    private int idFesta; //ok
    private String localizacao; //ok
    private double horario; //ok
    private String produtor; //ok
    private int capacidade; //ok
    private int faixaEtaria; //ok
    private String site; //ok
    private String descricao;

    public int getIdFesta() {
        return idFesta;
    }

    public void setIdFesta(int idFesta) {
        this.idFesta = idFesta;
    }

    public int getFaixaEtaria() {
        return faixaEtaria;
    }

    public void setFaixaEtaria(int faixaEtaria) {
        this.faixaEtaria = faixaEtaria;
    }

    
    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public double getHorario() {
        return horario;
    }

    public void setHorario(double horario) {
        this.horario = horario;
    }

    public String getProdutor() {
        return produtor;
    }

    public void setProdutor(String produtor) {
        this.produtor = produtor;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public void inserir() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Festa (idFesta, localizacao, horario, produtor, capacidade, faixaEtaria "
                + "site, descricao VALUES"
                + "(?,?,?,?,?,?,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.getIdFesta());
            preparedStatement.setString(2, this.getLocalizacao());
            preparedStatement.setDouble(3, this.getHorario());
            preparedStatement.setString(4, this.getProdutor());
            preparedStatement.setInt(5, this.getCapacidade());
            preparedStatement.setInt(6, this.getFaixaEtaria());
            preparedStatement.setString(7, this.getSite());
            preparedStatement.setString(8, this.getDescricao());
            
            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Festa inserida no banco");
        
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
                                                              
        String updateTableSQL = "update OO_Festa set localizacao = (?), horario = (?), produtor = (?)"
                + "capacidade = (?), faixaEtaria = (?), site = (?), descricao = (?), where idFesta = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            prepareStatement.setInt(8, this.getIdFesta());
            prepareStatement.setString(1, this.getLocalizacao());
            prepareStatement.setDouble(2, this.getHorario());
            prepareStatement.setString(3, this.getProdutor());
            prepareStatement.setInt(4, this.getCapacidade());
            prepareStatement.setInt(5, this.getFaixaEtaria());
            prepareStatement.setString(6, this.getSite());
            prepareStatement.setString(7, this.getDescricao());

            //execute insert SQL Statement
            prepareStatement.executeUpdate();
            
            System.out.println("Festa modificada!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean delete() { //ok
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM OO_Festa WHERE idFesta = ?";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setInt(1, this.getIdFesta());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            conexao.desconecta();
        }
        
        return true;
    }
    
    
}
