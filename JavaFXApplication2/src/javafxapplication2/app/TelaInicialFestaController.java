/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaInicialFestaController implements Initializable {

    @FXML
    private TextField emailTextField;
    @FXML
    private TextField senhaTextField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void cadastrarInicial() {

        try {
            CadastrarController.x = 1;
            Initializable trocaTela;
            trocaTela = Main.trocaTela("Cadastrar");

        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void visualizarInicial() {
        try {
            Initializable trocaTela = Main.trocaTela("visualizarFestas");

        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
