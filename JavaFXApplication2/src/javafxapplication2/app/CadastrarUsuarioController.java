/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import model.Usuario;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class CadastrarUsuarioController implements Initializable {

    @FXML
    private TextField emailTextField;
    @FXML
    private TextField senhaTextField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cadastrarUsuario() throws IOException {
        Usuario u = new Usuario();
        u.setEmail(emailTextField.getText());
        u.setSenha(senhaTextField.getText());
        u.inserirUsuario();
        Main.trocaTela("telaInicialSistema");
    }
}
