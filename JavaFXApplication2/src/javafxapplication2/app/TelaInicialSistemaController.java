/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaInicialSistemaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cadastrese() throws IOException {
        Main.trocaTela("cadastrarUsuario");

    }

    public void login() throws IOException {
        Initializable trocaTela = Main.trocaTela("logarUsuario");
    }
}
