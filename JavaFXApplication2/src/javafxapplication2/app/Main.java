/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Aluno
 */
public class Main extends Application {

    static Stage stage = new Stage();

    /**
     * método padrão de start FXML, aqui que começa a rodar a partir da primeira
     * tela.
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(CadastrarController.class.getResource("telaInicialSistema.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();

    }

    /**
     * método de troca de tela. É passado por parâmetro o nome da tela toda vez
     * que é chamado.
     *
     * @param s
     * @throws IOException
     */
    public static Initializable trocaTela(String s) throws IOException {

        FXMLLoader loader = new FXMLLoader(CadastrarController.class.getResource(s + ".fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        return loader.getController();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

}
