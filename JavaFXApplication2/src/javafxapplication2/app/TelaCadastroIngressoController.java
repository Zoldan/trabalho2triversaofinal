/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import static javafxapplication2.app.TelaCadastroIngressoController.ben;
import static javafxapplication2.app.TelaCadastroIngressoController.ing;
import model.Ingresso;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaCadastroIngressoController implements Initializable {

    /**
     *
     */
    static public ArrayList<Integer> ben = new ArrayList<>();

    @FXML
    private CheckBox foto;
    @FXML
    private CheckBox camiseta;
    @FXML
    private CheckBox open;
    @FXML
    private CheckBox copo;
    @FXML
    private CheckBox nenhum;
    @FXML
    private CheckBox bone;
    @FXML
    private CheckBox estacionamento;
    @FXML
    private CheckBox barraca;
    @FXML
    private TextField valorIng;

    public static Ingresso ing = new Ingresso();

    /**
     *
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    public void selecionarBeneficios() {
        int x;

        if (!ben.contains(1)) {
            if (foto.isSelected()) {

                ben.add(1);

            }
        }

        if (!ben.contains(2)) {
            if (camiseta.isSelected()) {
                ben.add(2);
            }
        }

        if (!ben.contains(3)) {
            if (open.isSelected()) {
                ben.add(3);
            }
        }

        if (!ben.contains(4)) {
            if (copo.isSelected()) {
                ben.add(4);
            }
        }

        if (!ben.contains(5)) {
            if (nenhum.isSelected()) {
                ben.add(5);
            }
        }

        if (!ben.contains(6)) {
            if (bone.isSelected()) {
                ben.add(6);
            }
        }

        if (!ben.contains(7)) {
            if (estacionamento.isSelected()) {
                ben.add(7);
            }
        }

        if (!ben.contains(8)) {
            if (barraca.isSelected()) {
                ben.add(8);
            }
        }

    }

    public void cadastrarIng() throws SQLException {

        ing.setValor(Double.parseDouble(valorIng.getText()));
        System.out.println(ing.getValor());

        try {

            Main.trocaTela("Cadastrar");

            // TODO
        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(ben);

    }

}
