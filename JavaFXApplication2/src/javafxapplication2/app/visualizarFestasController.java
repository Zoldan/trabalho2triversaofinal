/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import static javax.management.Query.value;
import model.Festa;
import model.Ingresso;

/**
 *
 * @author Emilly Zoldan
 */
public class visualizarFestasController implements Initializable {

    @FXML
    private TableView<Festa> tabelaFesta;
    @FXML
    private TableColumn<Festa, String> nome;
    @FXML
    private TableColumn<Festa, String> data;
    @FXML
    private TableColumn<Festa, String> horario;
    @FXML
    private Button vsinfo;
    @FXML
    private Button editar;
    @FXML
    private Button excluir;
    @FXML
    private Label local;
    @FXML
    private Label produtor;
    @FXML
    private Label capacidade;
    @FXML
    private Label faixaEtaria;
    @FXML
    private Label site;
    @FXML
    private Label atracaoP;
    @FXML
    private Label trilhaS;
    @FXML
    private Label tipoing;
    @FXML
    private Label beneficio1;
    @FXML
    private Label beneficio2;
    @FXML
    private Label beneficio3;
    @FXML
    private Label valoring;

    public static Festa dadosFesta = null;
    static public ObservableList<Festa> ps;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        editar.setVisible(false);
        excluir.setVisible(false);
        ps = tabelaFesta.getItems();

        try {
            adicionaFesta();

        } catch (SQLException ex) {
            Logger.getLogger(visualizarFestasController.class.getName()).log(Level.SEVERE, null, ex);
        }
        nome.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        data.setCellValueFactory(new PropertyValueFactory<>("data"));
        horario.setCellValueFactory(new PropertyValueFactory<>("horario"));

        this.tabelaFesta.setItems(ps);

    }

    private void adicionaFesta() throws SQLException {
        Festa f1 = new Festa();
        ArrayList<Festa> listaBanco = new ArrayList<>();
        for (Festa f : f1.getAll()) {

            listaBanco.add(f);

        }
        int i = 0;
        ///Copiar os valores de um vetor para outro
        for (i = 0; i < listaBanco.size(); i++) {
            ps.add(listaBanco.get(i));
        }
    }

    public void clickTabela() {

        editar.setVisible(false);
        excluir.setVisible(false);
        Festa f = new Festa();

        Festa selectedItem = tabelaFesta.getSelectionModel().getSelectedItem();
        int tipoVindo = f.procurar(selectedItem);

        if (Festa.a == 1) {
            editar.setVisible(true);
            excluir.setVisible(true);

        }
        local.setText(selectedItem.getLocalizacao());
        produtor.setText(selectedItem.getProdutor());
        capacidade.setText(Integer.toString(selectedItem.getCapacidade()));
        faixaEtaria.setText(Integer.toString(selectedItem.getFaixaEtaria()));
        site.setText(selectedItem.getSite());
        atracaoP.setText(selectedItem.getAtracaoPrincipal());
        trilhaS.setText(selectedItem.getTrilhaSonora());
        if (Festa.a == 1) {
            Ingresso i = new Ingresso();
            int tipo = i.pesquisarTipoIng(tipoVindo);

            if (tipo == 1) {
                tipoing.setText("VIP");

            } else if (tipo == 2) {
                tipoing.setText("Camarote");

            } else if (tipo == 3) {
                tipoing.setText("Pista");

            }
        }

    }

    public void excluir() {

        Festa selectedItem = tabelaFesta.getSelectionModel().getSelectedItem();
        tabelaFesta.getItems().remove(selectedItem);
        selectedItem.deleteFesta(Festa.idFesta);

    }

    public void editar() throws IOException {
        CadastrarController.x = 0;
        Festa selectedItem = tabelaFesta.getSelectionModel().getSelectedItem();
        dadosFesta = selectedItem;

        CadastrarController c = (CadastrarController) Main.trocaTela("Cadastrar");
        c.setDados();

    }

}
