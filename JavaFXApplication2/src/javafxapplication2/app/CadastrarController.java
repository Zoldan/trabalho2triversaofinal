/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import static java.util.Optional.empty;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import static javafxapplication2.app.TelaCadastroIngressoController.ing;
import model.Festa;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class CadastrarController implements Initializable {

    @FXML
    private TextField dataTextField;
    @FXML
    private TextField localizacaoTextField;
    @FXML
    private TextField horarioTextField;
    @FXML
    private TextField produtorTextField;
    @FXML
    private TextField capacidadeTextField;
    @FXML
    private TextField faixaEtariaTextField;
    @FXML
    private TextField siteTextField;
    @FXML
    private TextField atracaoPrincipalTextField;
    @FXML
    private TextField trilhaSonoraTextField;
    @FXML
    private TextField descricaoTextField;
    @FXML
    private Button vipButton;
    @FXML
    private Button pistaButton;
    @FXML
    private Button camaroteButton;
    @FXML
    private Button mudar;
    @FXML
    private Text textoIng;

    static public int x;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        voltaDados();
        if (this.x == 0) {
            setDados();
        }
        mudar.setVisible(false);
    }

    static ArrayList<String> dados2 = new ArrayList();
    static HashMap<String, String> dados = new HashMap();
    static public int tipoIng;

    public void cadastrarFestaIng() throws ParseException, SQLException, IOException {

        Festa f = new Festa();

        f.setAtracaoPrincipal(atracaoPrincipalTextField.getText());
        f.setCapacidade(Integer.parseInt(capacidadeTextField.getText()));
        f.setData(dataTextField.getText());
        f.setDescricao(descricaoTextField.getText());
        f.setFaixaEtaria(Integer.parseInt(faixaEtariaTextField.getText()));
        f.setHorario(horarioTextField.getText());
        f.setLocalizacao(localizacaoTextField.getText());
        f.setProdutor(produtorTextField.getText());
        f.setSite(siteTextField.getText());
        f.setTrilhaSonora(trilhaSonoraTextField.getText());
        f.inserirFesta();

        ing.inserirIngresso();

        Initializable trocaTela = Main.trocaTela("telaInicialFesta");

    }

    public void mudar() throws IOException {

        setDadosDiferenciado();

        Initializable trocaTela = Main.trocaTela("visualizarFestas");
    }

    public void salvaDados() {

        dados.put("localizacao", localizacaoTextField.getText());
        dados.put("horario", horarioTextField.getText());
        dados.put("produtor", produtorTextField.getText());
        dados.put("capacidade", capacidadeTextField.getText());
        dados.put("faixaEtaria", faixaEtariaTextField.getText());
        dados.put("site", siteTextField.getText());
        dados.put("atracaoPrincipal", atracaoPrincipalTextField.getText());
        dados.put("trilhaSonora", trilhaSonoraTextField.getText());
        dados.put("descricao", descricaoTextField.getText());
        dados.put("data", dataTextField.getText());

    }

    public void voltaDados() {
        localizacaoTextField.setText(dados.get("localizacao"));
        horarioTextField.setText(dados.get("horario"));
        produtorTextField.setText(dados.get("produtor"));
        capacidadeTextField.setText(dados.get("capacidade"));
        faixaEtariaTextField.setText(dados.get("faixaEtaria"));
        siteTextField.setText(dados.get("site"));
        atracaoPrincipalTextField.setText(dados.get("atracaoPrincipal"));
        trilhaSonoraTextField.setText(dados.get("trilhaSonora"));
        descricaoTextField.setText(dados.get("descricao"));
        dataTextField.setText(dados.get("data"));

    }

    public void selecionaIngressoVip() {
        tipoIng = 1;
        try {
            salvaDados();
            Main.trocaTela("telaCadastroIngresso");

            // TODO
        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selecionaIngressoCamarote() {
        tipoIng = 2;

        try {
            salvaDados();
            Main.trocaTela("telaCadastroIngresso");

            // TODO
        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selecionaIngressoPista() {
        tipoIng = 3;
        try {
            salvaDados();
            Main.trocaTela("telaCadastroIngresso");

            // TODO
        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setDados() {

        dataTextField.setText(visualizarFestasController.dadosFesta.getData());
        horarioTextField.setText(visualizarFestasController.dadosFesta.getHorario());
        localizacaoTextField.setText(visualizarFestasController.dadosFesta.getLocalizacao());
        produtorTextField.setText(visualizarFestasController.dadosFesta.getProdutor());
        capacidadeTextField.setText(Integer.toString(visualizarFestasController.dadosFesta.getCapacidade()));
        faixaEtariaTextField.setText(Integer.toString(visualizarFestasController.dadosFesta.getFaixaEtaria()));
        siteTextField.setText(visualizarFestasController.dadosFesta.getSite());
        atracaoPrincipalTextField.setText(visualizarFestasController.dadosFesta.getAtracaoPrincipal());
        trilhaSonoraTextField.setText(visualizarFestasController.dadosFesta.getTrilhaSonora());
        descricaoTextField.setText(visualizarFestasController.dadosFesta.getDescricao());
        mudar.setVisible(true);
        vipButton.setVisible(false);
        camaroteButton.setVisible(false);
        pistaButton.setVisible(false);
        textoIng.setVisible(false);

    }

    public void setDadosDiferenciado() {

        Festa dadosFesta = new Festa();
        dadosFesta.setData(dataTextField.getText());
        dadosFesta.setLocalizacao(localizacaoTextField.getText());
        dadosFesta.setHorario(horarioTextField.getText());
        dadosFesta.setProdutor(produtorTextField.getText());
        dadosFesta.setCapacidade(Integer.parseInt(capacidadeTextField.getText()));
        dadosFesta.setFaixaEtaria(Integer.parseInt(faixaEtariaTextField.getText()));
        dadosFesta.setSite(siteTextField.getText());
        dadosFesta.setAtracaoPrincipal(atracaoPrincipalTextField.getText());
        dadosFesta.setTrilhaSonora(trilhaSonoraTextField.getText());
        dadosFesta.setDescricao(descricaoTextField.getText());
        dadosFesta.update();
    }
}
