/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafxapplication2.app.Main;

/**
 *
 * @author Aluno
 */
public class Festa {

    public static int a;
    public static int idFesta;
    private String localizacao; //ok
    private String horario; //ok
    private String produtor; //ok
    private int capacidade; //ok
    private int faixaEtaria; //ok
    private String site; //ok
    private String descricao;
    private String atracaoPrincipal;
    private String trilhaSonora;
    private String data;

    public int getFaixaEtaria() {
        return faixaEtaria;
    }

    public void setFaixaEtaria(int faixaEtaria) {
        this.faixaEtaria = faixaEtaria;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;

    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProdutor() {
        return produtor;
    }

    public void setProdutor(String produtor) {
        this.produtor = produtor;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAtracaoPrincipal() {
        return atracaoPrincipal;
    }

    public void setAtracaoPrincipal(String atracaoPrincipal) {
        this.atracaoPrincipal = atracaoPrincipal;
    }

    public String getTrilhaSonora() {
        return trilhaSonora;
    }

    public void setTrilhaSonora(String trilhaSonora) {
        this.trilhaSonora = trilhaSonora;
    }

    public void inserirFesta() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Festa (idFesta, localizacao, horario, produtor, capacidade, faixaEtaria, "
                + "site, descricao, atracaoPrincipal, trilhaSonora, data, idUsuario) VALUES"
                + " ((select max(idFesta) from OO_Festa)+1,?,?,?,?,?,?,?,?,?,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.getLocalizacao());
            preparedStatement.setString(2, this.getHorario());
            preparedStatement.setString(3, this.getProdutor());
            preparedStatement.setInt(4, this.getCapacidade());
            preparedStatement.setInt(5, this.getFaixaEtaria());
            preparedStatement.setString(6, this.getSite());
            preparedStatement.setString(7, this.getDescricao());
            preparedStatement.setString(8, this.getAtracaoPrincipal());
            preparedStatement.setString(9, this.getTrilhaSonora());
            preparedStatement.setString(10, this.getData());
            preparedStatement.setInt(11, Usuario.getIdUsuario());
            System.out.println(this.getData());
            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Festa inserida no banco");

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public ArrayList<Festa> getAll() throws SQLException {

        String selectSQL = "select * from OO_Festa"; //where id> bla bla
        ArrayList<Festa> listaFesta = new ArrayList<>();
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        Statement st;
        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                Festa f = new Festa();
                f.setLocalizacao(rs.getString("localizacao"));
                f.setHorario(rs.getString("horario"));
                f.setProdutor(rs.getString("produtor"));
                f.setCapacidade(rs.getInt("capacidade"));
                f.setFaixaEtaria(rs.getInt("faixaEtaria"));
                f.setSite(rs.getString("site"));
                f.setDescricao(rs.getString("descricao"));
                f.setAtracaoPrincipal(rs.getString("atracaoPrincipal"));
                f.setTrilhaSonora(rs.getString("trilhaSonora"));
                f.setData(rs.getString("data"));

                listaFesta.add(f);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();

        }
        return listaFesta;
    }

    public int procurar(Festa festa) {

        /**
         *
         */
        String StringSelectSQL = "select OO_Festa.idFesta from OO_Festa inner join OO_UsuarioFesta"
                + " on OO_Festa.idUsuario = OO_UsuarioFesta.idUsuario where OO_Festa.descricao = ?"
                + " and OO_UsuarioFesta.idUsuario = ?";
        System.out.println(festa.getDescricao());
        System.out.println(Usuario.getIdUsuario());
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(StringSelectSQL);
            ps.setString(1, festa.getDescricao());
            ps.setInt(2, Usuario.getIdUsuario());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                Festa.idFesta = (rs.getInt("idFesta"));
                a = 1;
                System.out.println("OIII");
            } else {

                a = 0;
                System.out.println("oieee");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Festa.idFesta;
    }

    public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;

        String updateTableSQL = "update OO_Festa set localizacao = (?), horario = (?), produtor = (?),"
                + "capacidade = (?), faixaEtaria = (?), site = (?), descricao = (?), atracaoPrincipal = (?), trilhaSonora = (?), data = (?) where idFesta = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            prepareStatement.setString(1, this.getLocalizacao());
            prepareStatement.setString(2, this.getHorario());
            prepareStatement.setString(3, this.getProdutor());
            prepareStatement.setInt(4, this.getCapacidade());
            prepareStatement.setInt(5, this.getFaixaEtaria());
            prepareStatement.setString(6, this.getSite());
            prepareStatement.setString(7, this.getDescricao());
            prepareStatement.setString(8, this.getAtracaoPrincipal());
            prepareStatement.setString(9, this.getTrilhaSonora());
            prepareStatement.setString(10, this.getData());
            prepareStatement.setInt(11, idFesta);

            //execute insert SQL Statement
            prepareStatement.executeUpdate();

            System.out.println("Festa modificada!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean deleteFesta(int codigo) { 
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM OO_Ingresso where idFesta = ? ";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);

            System.out.println(codigo);
            ps.setInt(1, Festa.idFesta);
            ps.executeUpdate();
            System.out.println("deletado de ingresso");
        } catch (SQLException e) {
            e.printStackTrace();

        }
        String deletesql2 = "DELETE FROM OO_Festa where idFesta = ?";
        try {
            ps = dbConnection.prepareStatement(deletesql2);

            System.out.println(codigo);
            ps.setInt(1, Festa.idFesta);
            ps.executeUpdate();
            System.out.println("deletado de festa");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();
        }

        return true;
    }

}
