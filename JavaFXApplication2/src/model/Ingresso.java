/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static javafxapplication2.app.CadastrarController.tipoIng;
import static javafxapplication2.app.TelaCadastroIngressoController.ben;
import static javafxapplication2.app.TelaCadastroIngressoController.ing;
import model.Beneficio;

/**
 *
 * @author Aluno
 */
public class Ingresso {

    static Ingresso ingr = new Ingresso();
    private double valor;
    private int idTipoIngresso;
    private int idFesta;
    private int idBeneficio;
    private int idIngresso;

    public int getIdIngresso() {
        return idIngresso;
    }

    public void setIdIngresso(int idIngresso) {
        this.idIngresso = idIngresso;
    }

    public int getIdBeneficio() {
        return idBeneficio;
    }

    public void setIdBeneficio(int idBeneficio) {
        this.idBeneficio = idBeneficio;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdTipoIngresso() {
        return idTipoIngresso;
    }

    public void setIdTipoIngresso(int idTipoIngresso) {
        this.idTipoIngresso = idTipoIngresso;
    }

    public int getIdFesta() {
        return idFesta;
    }

    public void setIdFesta(int idFesta) {
        this.idFesta = idFesta;
    }

    public void inserirIngresso() throws SQLException { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Ingresso (valor, idTipoIngresso, idFesta, idIngresso) VALUES"
                + "(?,?,(select max(idFesta) from OO_Festa),(select max(idIngresso) from OO_Ingresso)+1)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            System.out.println();
            preparedStatement.setDouble(1, ing.getValor());
            preparedStatement.setInt(2, tipoIng);

            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Ingresso inserido no OO_INGRESSO");

        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Integer b : ben) {

            String insertTableSQL2 = "INSERT INTO OO_ING_BEN (idIngBen, idIngresso, idBeneficio, tipoIngresso) values"
                    + "((select max(idIngBen) from OO_ING_BEN)+1,(select max(idIngresso) from OO_Ingresso),?,?)";

            try {
                preparedStatement = dbConnection.prepareStatement(insertTableSQL2);
                preparedStatement.setInt(1, b);
                preparedStatement.setInt(2, tipoIng);
                preparedStatement.executeUpdate();
                System.out.println("Ingresso inserido no OO_ING_BEN");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
        //1              2                   3                4
        String updateTableSQL = "update Ingresso set valor = (?), tipo = (?) where codigo = ?";

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            prepareStatement.setDouble(1, this.getValor());
            prepareStatement.executeUpdate();

            System.out.println("Ingresso modificado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean deleteIngresso() { //ok
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM OO_ING_BEN WHERE idIngresso= ?";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setInt(1, this.getIdIngresso());
            ps.executeUpdate();
            System.out.println("deletado em oo_ing_ben");
        } catch (SQLException e) {
            e.printStackTrace();

        }
        String deleteTableSQL2 = "DELETE FROM OO_Ingresso where idIngresso = ?";
        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setInt(1, this.getIdIngresso());
            ps.executeUpdate();
            System.out.println("deletado em ingresso");
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            conexao.desconecta();
        }

        return true;
    }

    public void getAll() throws SQLException {

        String selectSQL = "SELECT * FROM Ingresso";
        ArrayList<Ingresso> listaIngresso = new ArrayList<>();
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Ingresso ingresso = new Ingresso();
                ingresso.setValor(rs.getDouble("valor"));
                listaIngresso.add(ingresso);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void getOne() throws SQLException {
        String selectSQL = "SELECT * FROM Ingresso WHERE codigo = ?";
        ConexaoBanco conexaoBanco = new ConexaoBanco();
        Connection dbConnection = conexaoBanco.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setValor(rs.getDouble("valor"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int pesquisarTipoIng(int id) {

        String StringSelectSQL = "select idtipoIngresso from OO_Ingresso inner join OO_Festa on OO_Ingresso.idFesta = OO_Festa.idFesta where OO_Festa.idFesta = ?";

        //System.out.println(festa.getDescricao());
        //  System.out.println(Usuario.getIdUsuario());
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;

        try {
            ps = dbConnection.prepareStatement(StringSelectSQL);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {

                ingr.setIdTipoIngresso((rs.getInt("idTipoIngresso")));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingr.idTipoIngresso;
    }

}
