/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.Initializable;
import javafxapplication2.app.Main;

/**
 *
 * @author Emilly Zoldan
 */
public class Usuario {

    static private int idUsuario;
    private String email;
    private String senha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public static int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void inserirUsuario() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_UsuarioFesta (idUsuario, email, senha) VALUES"
                + " ((select max(idUsuario) from OO_UsuarioFesta)+1,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.getEmail());
            preparedStatement.setString(2, this.getSenha());

            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Usuario inserido no banco");

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void load() throws IOException {
        String selectSQL = "SELECT * FROM OO_UsuarioFesta WHERE email = ? and senha = ?";
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, this.getEmail());
            ps.setString(2, this.getSenha());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                this.setIdUsuario(rs.getInt("idUsuario"));
                this.setEmail(rs.getString("email"));
                this.setSenha(rs.getString("senha"));
                Initializable trocaTela = Main.trocaTela("telaInicialFesta");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
