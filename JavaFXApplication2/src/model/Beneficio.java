/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Emilly Zoldan
 */
public class Beneficio {

    private String nomeBen;
    private int idBen;

    public String getNomeBen() {
        return nomeBen;
    }

    public void setNomeBen(String nomeBen) {
        this.nomeBen = nomeBen;
    }

    public int getIdBen() {
        return idBen;
    }

    public void setIdBen(int idBen) {
        this.idBen = idBen;
    }

}
